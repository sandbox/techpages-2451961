<?php
/**
 * @file
 * Theme and preprocess functions for the Podlove Player module.
 */

/**
 * Preprocess function for a player.
 */
function template_preprocess_podlove_player(&$vars) {
  drupal_add_css(libraries_get_path('podlove-web-player') . '/static/podlove-web-player.css');
  drupal_add_js(libraries_get_path('podlove-web-player') . '/libs/html5shiv.js');
  drupal_add_js(libraries_get_path('podlove-web-player') . '/static/podlove-web-player.js');

  // Determine a unique player ID.
  $vars['player_id'] = uniqid('plplayer-');
  $vars['source_file'] = file_create_url($vars['items'][0]['uri']);

  // If the player is used in File Entity Module, the node is a reference.
  $node_ref = ($vars['entity_type'] == 'node' ? $vars['entity'] :
    $vars['entity']->referencing_entity);

  // Get poster image.
  $file_id = 0;
  $file = NULL;
  if ($vars['settings']['metadata']['poster_fid'] != 0) {
    $file_id = $vars['settings']['metadata']['poster_fid'];
  }
  else {
    $file = podlove_player_get_string($vars['settings']['metadata']['poster_token'], $node_ref);
    if (is_numeric($file)) {
      $file_id = $file;
    }
    elseif ($file == '' || $file == '<none>') {
      $file = NULL;
    }
  }
  if ($file_id > 0) {
    $file = file_load($file_id);
    if ($file == '') {
      $file = NULL;
    }
    else {
      if (substr($file->filemime, 0, 6) == 'image/') {
        $file = file_create_url($file->uri);
      }
      else {
        $file = NULL;
      }
    }
  }

  // Get all options and mount in a JSon array.
  $tmp = podlove_player_get_string($vars['settings']['metadata']['title'], $node_ref);
  if ($tmp != '') {
    $vars['options']['title'] = $tmp;
  }
  $tmp = podlove_player_get_string($vars['settings']['metadata']['subtitle'], $node_ref);
  if ($tmp != '') {
    $vars['options']['subtitle'] = $tmp;
  }
  $tmp = podlove_player_get_string($vars['settings']['metadata']['permalink'], $node_ref);
  if ($tmp != '') {
    $vars['options']['permalink'] = $tmp;
  }
  if (!is_null($file)) {
    $vars['options']['poster'] = $file;
  }
  $tmp = podlove_player_get_string($vars['settings']['metadata']['summary'], $node_ref);
  if ($tmp != '') {
    $vars['options']['summary'] = $tmp;
  }
  $vars['options']['startVolume']
    = (string) intval($vars['settings']['start_volume']) / 100;
  $vars['options']['alwaysShowHours']
    = ($vars['settings']['controls']['show_hours'] ? TRUE : FALSE);
  $vars['options']['summaryVisible']
    = ($vars['settings']['controls']['show_summary'] ? TRUE : FALSE);
  $vars['options']['timecontrolsVisible']
    = ($vars['settings']['controls']['show_timecontrols'] ? TRUE : FALSE);
  $vars['options']['sharebuttonsVisible']
    = ($vars['settings']['controls']['show_sharebuttons'] ? TRUE : FALSE);
  $vars['options']['chaptersVisible'] = FALSE;

  $vars['options_json'] = drupal_json_encode($vars['options']);
}

/**
 * Get the tokenized string.
 */
function podlove_player_get_string($str_in, $node_in) {
  $temp = trim(token_replace($str_in, array('node' => $node_in)));
  if ($temp == "<none>") {
    $temp = '';
  }
  $my_allowed_tags = array('a', 'em', 'strong', 'cite', 'blockquote', 'code',
    'ul', 'ol', 'li', 'dl', 'dt', 'dd');
  return filter_xss($temp, $my_allowed_tags);
}
