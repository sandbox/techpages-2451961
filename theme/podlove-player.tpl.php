<?php

/**
 * @file
 * Provide the HTML output for a Podlove Player interface.
 *
 * Variables passed by $args in template_preprocess_podlove_player functions
 * Just save the variable inside $args and it will be available here.
 *
 * using now:
 *   $player_id = string     unique ID for player
 *   $source_file = url      audio file
 *   $options_json = string  settings to player
 */
?>

<div class="podloveplayer">
  <audio id="<?php print $player_id; ?>">
    <source src="<?php print $source_file; ?>" type="audio/mpeg"></source>
  </audio>
  <script>
    jQuery('#<?php print $player_id; ?>').podlovewebplayer(<?php print $options_json; ?>);
  </script>
</div>
