<?php
/**
 * @file
 * Administrative page for the Podlove Player module.
 */

/**
 * Provides the Podlove Player defaults settings form.
 */
function podlove_player_settings_form($form, &$form_state) {
  $form['podlove_player_metadata'] = array(
    '#type' => 'fieldset',
    '#title' => t('Metadata Defaults'),
  );

  $form['podlove_player_metadata']['podlove_player_title'] = array(
    '#type' => 'textfield',
    '#title' => t('Title'),
    '#default_value' => variable_get('podlove_player_title', '[site:name]'),
    '#description' => t('The Title on Podlove Player metadata.') .
      ' ' . t('Default: <code>[site:name]</code>'),
    '#element_validate' => array('token_element_validate'),
    '#after_build' => array('token_element_validate'),
    '#token_types' => array('node', 'file'),
  );
  $form['podlove_player_metadata']['podlove_player_subtitle'] = array(
    '#type' => 'textfield',
    '#title' => t('Subtitle'),
    '#default_value' => variable_get('podlove_player_subtitle', '[node:title]'),
    '#description' => t('The Subtitle on Podlove Player metadata.') .
      ' ' . t('Default: <code>[node:title]</code>'),
    '#element_validate' => array('token_element_validate'),
    '#after_build' => array('token_element_validate'),
    '#token_types' => array('node', 'file'),
  );
  $form['podlove_player_metadata']['podlove_player_permalink'] = array(
    '#type' => 'textfield',
    '#title' => t('Link in Title metadata'),
    '#default_value' => variable_get('podlove_player_permalink', '[node:url]'),
    '#description' => t('The link in Title metadata. <strong>This link
      is used on shared buttons too.</strong>') . ' ' .
      t('Default: <code>[node:url]</code>'),
    '#element_validate' => array('token_element_validate'),
    '#after_build' => array('token_element_validate'),
    '#token_types' => array('node', 'file'),
  );
  $form['podlove_player_metadata']['podlove_player_summary'] = array(
    '#type' => 'textfield',
    '#title' => t('Summary'),
    '#default_value' => variable_get('podlove_player_summary', '<none>'),
    '#description' => t('The Summary text inside Podlove Player.') .
      ' ' . t('Default: <code>&lt;none&gt;</code>'),
    '#element_validate' => array('token_element_validate'),
    '#after_build' => array('token_element_validate'),
    '#token_types' => array('node', 'file'),
  );
  $form['podlove_player_metadata']['poster'] = array(
    '#type' => 'fieldset',
    '#title' => t('Cover image'),
  );
  $form['podlove_player_metadata']['poster']['podlove_player_poster_fid'] = array(
    '#type' => 'managed_file',
    '#title' => t('Cover image'),
    '#description' => t('Cover Image'),
    '#default_value' => variable_get('podlove_player_poster_fid', 0),
  );
  $form['podlove_player_metadata']['poster']['podlove_player_poster_token'] = array(
    '#type' => 'textfield',
    '#title' => t('Cover image by token parameters'),
    '#default_value' => variable_get('podlove_player_poster_token', '<none>'),
    '#description' => t('The Token setting for Cover Image.') .
      ' ' . t('Default: <code>&lt;none&gt;</code>'),
    '#element_validate' => array('token_element_validate'),
    '#after_build' => array('token_element_validate'),
    '#token_types' => array('node', 'file'),
  );
  $form['token_help'] = array(
    '#title' => t('Replacement patterns'),
    '#type' => 'fieldset',
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );
  $form['token_help']['help'] = array(
    '#theme' => 'token_tree',
    '#token_types' => array('node', 'site'),
  );

  return system_settings_form($form);
}
