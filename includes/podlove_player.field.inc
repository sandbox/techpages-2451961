<?php
/**
 * @file
 * Field integration.
 *
 * This module provides functionality for loading the Podlove Web Player
 *  library and formatters for File Fields.
 */

/**
 * Implements hook_field_formatter_info().
 */
function podlove_player_field_formatter_info() {

  return array(
    'podlove_player' => array(
      'label' => t('Podlove - Player'),
      'field types' => array('file'),
      'description' => t('Display file fields in Podlove player.'),
      'settings' => array(
        'metadata' => array(
          'title' => variable_get('podlove_player_title', '[site:name]'),
          'subtitle' => variable_get('podlove_player_subtitle', '[node:title]'),
          'permalink' => variable_get('podlove_player_permalink', '[node:url]'),
          'summary' => variable_get('podlove_player_summary', '<none>'),
          'poster_fid' => variable_get('podlove_player_poster_fid', 0),
          'poster_token' => variable_get('podlove_player_poster_token', '<none>'),
        ),
        'start_volume' => '80',
        'controls' => array(
          'show_hours' => TRUE,
          'show_summary' => FALSE,
          'show_timecontrols' => TRUE,
          'show_sharebuttons' => FALSE,
        ),
        'length' => NULL,
        'show_chapters' => FALSE,
        'chapters' => NULL,
        'downloads' => NULL,
        'width' => NULL,
      ),
      'view callback' => 'podlove_player_field_formatter_view',
      'settings callback' => 'podlove_player_field_formatter_settings_form',
    ),
  );
}

/**
 * Implements hook_field_formatter_settings_form().
 */
function podlove_player_field_formatter_settings_form($field, $instance, $view_mode, $form, &$form_state) {
  $display = $instance['display'][$view_mode];
  $settings = $display['settings'];
  $form = array();

  if ($display['type'] == 'podlove_player') {
    $form['metadata'] = array(
      '#title' => t('Podlove metadata'),
      '#type' => 'fieldset',
      '#collapsible' => TRUE,
      '#collapsed' => TRUE,
      '#description' => t('Data showed inside Podlove Player.') . ' <strong>' .
        t('To use the small player, left all these fields blank.') . '</strong>',
    );
    $form['metadata']['title'] = array(
      '#type' => 'textfield',
      '#title' => t('Title on Podlove Player'),
      '#description' => t('Title inside Podlove Player (accepts token).') . ' ' .
        t("Suggestions: Your Podcast's name. Your site's name token."),
      '#default_value' => $settings['metadata']['title'],
      '#element_validate' => array('token_element_validate',
        'podlove_player_show_change_warning'),
      '#after_build' => array('token_element_validate'),
      '#token_types' => array('node', 'file'),
    );
    $form['metadata']['subtitle'] = array(
      '#type' => 'textfield',
      '#title' => t('Subtitle on Podlove Player'),
      '#description' => t('Subtitle inside Podlove Player (accepts token).') .
        ' ' . t('Suggestions: Your Episode name token. The node title token.'),
      '#default_value' => $settings['metadata']['subtitle'],
      '#element_validate' => array('token_element_validate',
        'podlove_player_show_change_warning'),
      '#after_build' => array('token_element_validate'),
      '#token_types' => array('node', 'file'),
    );
    $form['metadata']['permalink'] = array(
      '#type' => 'textfield',
      '#title' => t('Permalink on Podlove Player'),
      '#description' => t("The Title's link inside Podlove, also used
        on shared buttons. (accepts token)") . ' ' .
        t('Suggestions: Your node URL token. Your site URL.'),
      '#default_value' => $settings['metadata']['permalink'],
      '#element_validate' => array('token_element_validate',
        'podlove_player_show_change_warning'),
      '#after_build' => array('token_element_validate'),
      '#token_types' => array('node', 'file'),
    );
    $form['metadata']['summary'] = array(
      '#type' => 'textfield',
      '#title' => t('Summary on Podlove Player'),
      '#description' => t('Summary text inside Podlove Player. (accepts token)') .
        ' ' . t("Suggestions: Your node's teaser token.
        Your default Podcast description."),
      '#default_value' => $settings['metadata']['summary'],
      '#element_validate' => array('token_element_validate',
        'podlove_player_show_change_warning'),
      '#after_build' => array('token_element_validate'),
      '#token_types' => array('node', 'file'),
    );
    $form['metadata']['poster_fid'] = array(
      '#type' => 'managed_file',
      '#title' => t('Cover image'),
      '#description' => t('Cover Image'),
      '#default_value' => $settings['metadata']['poster_fid'],
    );
    $form['metadata']['poster_token'] = array(
      '#type' => 'textfield',
      '#title' => t('Cover image by token parameters'),
      '#description' => t('Token enabled setting for Cover Image.') . ' ' .
        t("Suggestions: Your episode's image token. Your default Podcast logo."),
      '#default_value' => $settings['metadata']['poster_token'],
      '#element_validate' => array('token_element_validate',
        'podlove_player_show_change_warning'),
      '#after_build' => array('token_element_validate'),
      '#token_types' => array('node', 'file'),
    );
    $form['metadata']['token_help'] = array(
      '#title' => t('Replacement patterns'),
      '#type' => 'fieldset',
      '#collapsible' => TRUE,
      '#collapsed' => TRUE,
    );
    $form['metadata']['token_help']['help'] = array(
      '#theme' => 'token_tree',
      '#token_types' => array('node', 'site'),
    );
    $form['start_volume'] = array(
      '#type' => 'textfield',
      '#title' => t('Start volume'),
      '#description' => t('Start Volume of Player = 0 to 100'),
      '#default_value' => $settings['start_volume'],
      '#field_suffix' => '%',
      '#maxlength' => 3,
      '#size' => 3,
      '#element_validate' => array('podlove_player_volume_check',
        'podlove_player_show_change_warning'),
    );
    $form['controls'] = array(
      '#title' => t('Podlove control buttons'),
      '#type' => 'fieldset',
      '#collapsible' => TRUE,
      '#collapsed' => TRUE,
      '#description' => t('Controls and features from Podlove Player.'),
    );
    $form['controls']['show_hours'] = array(
      '#type' => 'checkbox',
      '#title' => t('Always show hours'),
      '#description' => t('Show time as hh:mm:ss'),
      '#default_value' => $settings['controls']['show_hours'],
      '#element_validate' => array('podlove_player_show_change_warning'),
    );
    $form['controls']['show_summary'] = array(
      '#type' => 'checkbox',
      '#title' => t('Summary visible'),
      '#description' => t('When exists, summary visible by default?'),
      '#default_value' => $settings['controls']['show_summary'],
      '#element_validate' => array('podlove_player_show_change_warning'),
    );
    $form['controls']['show_timecontrols'] = array(
      '#type' => 'checkbox',
      '#title' => t('Timecontrols visible'),
      '#description' => t('Timecontrols (FF / RW) visible by default?'),
      '#default_value' => $settings['controls']['show_timecontrols'],
      '#element_validate' => array('podlove_player_show_change_warning'),
    );
    $form['controls']['show_sharebuttons'] = array(
      '#type' => 'checkbox',
      '#title' => t('Sharebuttons visible'),
      '#description' => t('Sharebuttons visible by default?'),
      '#default_value' => $settings['controls']['show_sharebuttons'],
      '#element_validate' => array('podlove_player_show_change_warning'),
    );
  }
  return $form;
}

/**
 * Show message "need save".
 */
function podlove_player_show_change_warning($form, &$form_state) {
  drupal_set_message(t('* Changes made in Podlove settings will not be saved
    until the form is submitted.'), 'warning', FALSE);
}

/**
 * Implements hook_field_formatter_view().
 */
function podlove_player_field_formatter_view($entity_type, $entity, $field, $instance, $langcode, $items, $display) {
  $element = array();
  switch ($display['type']) {
    case 'podlove_player':
      if (!empty($items)) {
        $output = theme(
          'podlove_player',
          array(
            'entity_type' => $entity_type,
            'entity' => $entity,
            'field_name' => $instance['field_name'],
            'items' => $items,
            'settings' => $display['settings'],
          )
        );
      }
      else {
        return array();
      }
      $element[0] = array(
        '#markup' => $output,
      );
      break;
  }

  return $element;
}

/**
 * Element validation to check volume is between 0 and 100.
 */
function podlove_player_volume_check($element, &$form_state) {
  $temp = trim($element['#value']);
  if (!is_numeric($temp)) {
    form_set_error('plp_startVolume', t('Volume must be numeric (0 to 100)'));
  }
  elseif (!ctype_digit($temp)) {
    form_set_error('plp_startVolume', t('Volume must be integer from 0 to 100'));
  }
  elseif (intval($temp) > 100) {
    form_set_error('plp_startVolume', t('Maximum Start Volume is 100'));
  }
  $element['#value'] = (string) intval($temp);
}

/**
 * Implements hook_field_formatter_settings_summary().
 */
function podlove_player_field_formatter_settings_summary($field, $instance, $view_mode) {
  $display = $instance['display'][$view_mode];
  $settings = $display['settings'];
  $sumtext = '';

  if ($display['type'] == 'podlove_player') {
    $show = '';
    $hide = '';
    ($settings['controls']['show_hours'] ? $show .= t('Hours') . ', ' :
      $hide .= t('Hours') . ', ');
    ($settings['controls']['show_summary'] ? $show .= t('Summary') . ', ' :
      $hide .= t('Summary') . ', ');
    ($settings['controls']['show_timecontrols'] ? $show .= t('Time controls') .
      ', ' : $hide .= t('Time controls') . ', ');
    ($settings['controls']['show_sharebuttons'] ? $show .= t('Share buttons') .
      ', ' : $hide .= t('Share buttons') . ', ');
    $show = ($show == '' ? t('Nothing') : substr($show, 0, -2));
    $hide = ($hide == '' ? t('Nothing') : substr($hide, 0, -2));
    $sumtext = t('Title') . ': <i>' .
      substr(check_plain($settings['metadata']['title']), 0, 20) . '</i><br>';
    $sumtext .= t('Subtitle') . ': <i>' .
      substr(check_plain($settings['metadata']['subtitle']), 0, 20) . '</i><br>';
    $sumtext .= t('Link to') . ': <i>' .
      substr(check_plain($settings['metadata']['permalink']), 0, 20) . '</i><br>';
    $sumtext .= t('Summary') . ': <i>' .
      substr(check_plain($settings['metadata']['summary']), 0, 20) . '</i><br>';
    $sumtext .= t('Image') . ': <i>' .
      substr(check_plain($settings['metadata']['poster_token']), 0, 20) .
      ' OR file#' . $settings['metadata']['poster_fid'] . '</i><br>';
    $sumtext .= t('Start Volume') . ': <i>' . $settings['start_volume'] . '%</i><br>';
    $sumtext .= t('Show') . ': <i>' . $show . '</i><br>';
    $sumtext .= t('Hide') . ': <i>' . $hide . '</i>';
  }

  return $sumtext;
}
