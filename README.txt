CONTENTS OF THIS FILE
---------------------
  
* Introduction
* Requirements
* Recommended modules
* Installation
* Configuration
* Use example
* Maintainers


INTRODUCTION
------------
The Podlove Player module provides a ease and powerful way to use the Podlove
Web Player in audio files. Ideal to Podcast.

Podlove Web Player is a HTML5 based audio/video player, focused on podcasts
and similar media blogs. It supports chapters, deeplinks, captions, social
media buttons and more, fully optimized and extended for the specific needs
of podcasters.

* For a full description of the module, visit the project page:
	https://www.drupal.org/sandbox/techpages/2451961

* To submit bug reports and feature suggestions, or to track changes:
	https://www.drupal.org/project/issues/2451961


REQUIREMENTS
------------
This module requires the following modules:

* JQuery Update (https://www.drupal.org/project/jquery_update)
* Libraries API (https://www.drupal.org/project/libraries)
* Token (https://www.drupal.org/project/token)


RECOMMENDED MODULES
-------------------
* Fielder (https://www.drupal.org/project/fielder)
	When enabled, you can insert the player inside body, even without any
	wysiwyg editor.
* Media 2.x (https://www.drupal.org/project/media)
	With Media and File Entity (required by Media) you can manage your media
	(audio and images in our case) with visual interface and reusability.


INSTALLATION
------------

1. Install as you would normally install a contributed Drupal module.
	See: https://drupal.org/documentation/install/modules-themes/modules-7
	for further information.

2. Download the Podlove Web Player WP Plugin Version (recommended) at
	http://wordpress.org/plugins/podlove-web-player OR Podlove Standalone
	Version at https://github.com/podlove/podlove-web-player/tree/standalone
	and extract its contents in your libraries directory (usually
	/sites/all/libraries).
	The path to JS main file must be:
	libraries/podlove-web-player/static/podlove-web-player.js

3. Done. To check, you can go to Status Report page (admin/reports/status)
	and look for Podlove Player Library.

At this point, you can go in Manage Display of any Content Type and select
"Podlove - Player" as Format of any File Field. Of course, the Podlove Player
only will show audio files. Everything else will be ignored.


CONFIGURATION
-------------

* This module has no configurations in user permissions. Everyone with rights
	to add/edit content types can configure Podlove Player on file fields.

* In Configuration/Media/Podlove Player (/admin/config/media/podlove_player)
	you can choose the default configuration when a new Content Type file
	field is created. All this settings can be overwrite in File Field Format
	settings; it's just default values.

* When you have a file field in any content type, you can configure it to be
	rendered (showed) with Podlove Player in Manage Display / Format. At right
	side, a "gear" button open the Configuration Form to customize that
	particular field with your needs.

*** The configuration items are:
  - Title. The first line on Podlove Player, with link (if set to).
	  Options:
		- Fixed text: all node with this field will have the same text.
		- Token: the text will be the content of the token.
		- "<none>" _or_ leave this blank: no text will be showed.
	  default: "[site:name]"

  - Subtitle. The second line on Podlove Player, below title.
	  Options:
		- Fixed text: all node with this field will have the same text.
		- Token: the text will be the content of the token.
		- "<none>" _or_ leave this blank: no text will be showed.
	  default: "[node:title]"

  - Permalink. The URL linked in Title.
	  Options:
		- Fixed link: all node with this field will have the same link.
		- Token: the link will be the content of the token.
		- "<none>" _or_ leave this blank: no link in Title.
	  default: "[node:url]"

  - Summary. The text showed inside Podlove Display.
	  Options:
		- Fixed text: all node with this field will have the same text.
		- Token: the text will be the content of the token.
		- "<none>" _or_ leave this blank: no text will be showed.
	  default: "<none>"

  - Cover Image. The image showed in right side of Player Button.
	  Options:
		- on Image: you can load a image file or leave this blank.
		- on Tokenized:
		  - Fixed fid: a URL to a image or a valid FID on system.
		  - Token: the image will be the content of the token.
		  - "<none>" _or_ leave this blank: no image will be showed.
	  default: (no image loaded, and "<none>" on Tokenized)
	  suggestion: "[node:field_image]" to show your node image.

  - Start volume. The default volume when player is showed.
	  Options:
		- any integer from 0 (mute) to 100 (louder).
	  default: 80

  - Always show hours. Show hour on time values (hh:mm:ss instead mm:ss).
	  default: TRUE

  - Summary visible. Show summary on start or wait user press INFO button.
	  default: FALSE

  - Time controls visible. Show FF and RW buttons below timebar by default.
	  default: TRUE

  - Share buttons visible. Show Social Media Buttons on player start.
	  default: FALSE


USE EXAMPLE
-----------

 1. Create a new Content Type with fields:
    * Title - default
    * Body - default
    * Image - normal image file
    * Podcast - Type: File - Widget: File 
              - don't forget to set in "Allowed file extensions": mp3

 2. In Manage Display, choose "Podlove - Player" to FORMAT Podcast field.

 3. Choose Field Display Settings (gear button on right side) and set:
    * Summary on Podlove Player: "[node:summary]"
    * Cover image by token parameters: "[node:field_image]"

 4. Add new node from your Content Type with a image, body text and a mp3 file.

With this settings, the Podlove Player will show:
 * Your site name as title
 * Your node URL will be the link on title
 * Your node title as subtitle
 * Your node image as cover image
 * Your body text as summary (to see, click on summary button)


MAINTAINERS
-----------
Current maintainers:
* Edvaldo Biancarelli (TechPages) - https://drupal.org/user/708676
